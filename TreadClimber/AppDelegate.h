//
//  AppDelegate.h
//  TreadClimber
//
//  Created by Elias Esquivel on 10/11/15.
//  Copyright © 2015 Elias Esquivel. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

