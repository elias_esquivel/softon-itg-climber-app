//
//  CustomTableViewCell.m
//  TreadClimber
//
//  Created by Elias Esquivel on 10/11/15.
//  Copyright © 2015 Elias Esquivel. All rights reserved.
//

#import "CustomTableViewCell.h"

@interface CustomTableViewCell()
@property (weak, nonatomic) IBOutlet UIView *containerView;
@property (weak, nonatomic) IBOutlet UILabel *lbl;

@end

@implementation CustomTableViewCell


- (void)awakeFromNib {
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
